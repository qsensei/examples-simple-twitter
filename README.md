# Simple Twitter

This is a simple example of the capabalities of Fuse using a data model that
many people should be familiar with: tweets and twitter users.

## Setup Instance

```
  curl -XPUT $FUSEURL/instances/fuse0 \
    -H x-user-id:admin \
    -H content-type:application/zip \
    -T schema.zip
```

## Start the Instance

```
  curl -XPOST $FUSEURL/instances/fuse0 \
    -H x-user-id:admin \
    -H content-type:application/json \
    -d '{"action": "start"}'
```

## Insert Users

```
  curl -XPOST $FUSEURL/tasks/types/update \
    -H x-user-id:admin \
    -H content-type:application/json \
    -d @data/users.json
```

## Insert Tweets

```
  curl -XPOST $FUSEURL/tasks/types/update \
    -H x-user-id:admin \
    -H content-type:application/json \
    -d @data/tweets.json
```