from subprocess import check_call, check_output
from time import sleep
import json
import os

import requests

up = os.path.dirname
DATA = os.path.join(up(up(__file__)), 'data')
SCHEMA = os.path.join(up(up(__file__)), 'schema.zip')


class TestBase:

    fuseurl = 'http://%s' % check_output(('boot2docker', 'ip')).strip()

    def setup(self):
        self.container_id = check_output(
            ('docker', 'run', '-d', '-p', '80:80', 'fuse')).strip()
        sleep(5)

    def teardown(self):
        with open(os.devnull, 'w') as f:
            check_call(('docker', 'kill', self.container_id),
                       stdout=f)
            check_call(('docker', 'wait', self.container_id),
                       stdout=f)
            check_call(('docker', 'rm', self.container_id),
                       stdout=f)

    def _request(self, method, path, **kws):
        r = requests.request(method, '%s%s' % (self.fuseurl, path), **kws)
        r.raise_for_status()
        return r

    def _wait(self, taskid):
        while True:
            t = self._request('GET', '/tasks/stats/%s' % taskid,
                              headers={'x-user-id': 'admin'}).json()
            if t['done']:
                break
            sleep(0.5)
        assert t['errors']['total'] == 0
        if 'index_task' in t:
            self._wait(t['index_task']['task_id'])
        return t


class TestFuseInstanceReady(TestBase):

    def setup(self):
        TestBase.setup(self)
        # setup the instance
        with open(SCHEMA) as f:
            self._request('PUT', '/instances/fuse0', data=f,
                          headers={'content-type': 'application/zip'})
        self._request('POST', '/instances/fuse0', json={'action': 'start'})
        sleep(5)
        status = self._request('GET', '/instances/fuse0').json()
        assert status['ready']

    def test_data_import(self):
        # import test data
        for dataset in ('users.json', 'tweets.json'):
            with open(os.path.join(DATA, dataset)) as f:
                tweets = json.load(f)
            r = self._request('POST', '/tasks/types/update',
                              json=tweets, headers={'x-user-id': 'admin'})
            # Wait for task to complete
            task_id = requests.get(r.headers['location'],
                                   headers={'x-user-id': 'admin'}
                                   ).json()['task_id']
            status = self._wait(task_id)
            # Check object numbers
            if dataset == 'users.json':
                assert status['result']['updated']['created'] == 2
            elif dataset == 'tweets.json':
                assert status['result']['updated']['created'] == 5
        # verify hashtags facet
        facet = self._request('GET', '/facets/hashtag').json()
        assert facet['name'] == 'hashtag'
        expected = {'CRUNCHIES', 'TCMeetup'}
        assert expected == {x['value'] for x in facet['items']}
        # verify authors facet
        facet = self._request('GET', '/facets/author').json()
        assert facet['name'] == 'author'
        expected = {'techcrunch', 'TechCrunch', 'Wall Street Journal', 'WSJ'}
        assert expected == {x['value'] for x in facet['items']}
